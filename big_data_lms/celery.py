import os
from celery import Celery

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'big_data_lms.settings')

app = Celery('big_data_lms')
app.config_from_object('django.conf:settings')

app.autodiscover_tasks()
