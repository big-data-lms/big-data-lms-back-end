import os

from django.http import HttpResponse
from django.template import loader
from rest_framework import viewsets, status
from rest_framework.decorators import action
from rest_framework.response import Response

from lms.helpers.instances_manipulation_helpers import check_and_revoke_notification_and_stop_tasks
from lms.models import Course, Lesson, PassedLesson, RoleArchive
from accounts.permissions import is_owner_or_read_only
from lms.serializers import LessonSerializer, PassedLessonSerializer, \
    PassedLessonEmptySerializer, PassedLessonStatusSerializer, \
    VariablesConstructorSerializer, CourseSerializer, RolesListConstructorSerializer, RolesArchivesSerializer
from lms.tasks import run_task, stop_task, terminate_task, notification_task


class CourseView(viewsets.ModelViewSet):
    queryset = Course.objects.all().select_related('teacher').order_by('-id')
    serializer_class = CourseSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.teacher),)
    search_fields = ('name',)


class PassedLessonView(viewsets.ModelViewSet):
    queryset = PassedLesson.objects.all().select_related('lesson', 'lesson__ansible_template').order_by('-id')
    serializer_class = PassedLessonSerializer
    filterset_fields = ['is_notification_active', 'student', 'status']

    @action(methods=['PUT'], detail=True, serializer_class=PassedLessonEmptySerializer)
    def read_notification(self, request, pk=None):
        passed_lesson = self.get_object()

        if passed_lesson.is_notification_active:
            check_and_revoke_notification_and_stop_tasks(passed_lesson)

            notify_task_async_result = notification_task.apply_async(
                (passed_lesson.pk,),
                countdown=int(passed_lesson.lesson.notification_timer.total_seconds())
            )
            passed_lesson.notification_task_id = notify_task_async_result.id

            passed_lesson.is_notification_active = False
            passed_lesson.save()

            return Response()

        return Response(data='This passed lesson does not have unread notifications',
                        status=status.HTTP_400_BAD_REQUEST)

    @action(methods=['PUT'], detail=True, serializer_class=PassedLessonStatusSerializer)
    def manipulate_instances(self, request, pk=None):
        passed_lesson = self.get_object()
        if passed_lesson.status in (PassedLesson.PassedLessonStatus.TERMINATED, PassedLesson.PassedLessonStatus.FAILED):
            return Response(data='You can not manipulate with terminated or failed instances',
                            status=status.HTTP_400_BAD_REQUEST)
        desired_status = request.data['status']
        if desired_status == PassedLesson.PassedLessonStatus.RUNNING:
            run_task.delay(passed_lesson.pk)
        elif desired_status == PassedLesson.PassedLessonStatus.STOPPED:
            stop_task.delay(passed_lesson.pk)
        elif desired_status == PassedLesson.PassedLessonStatus.TERMINATED:
            terminate_task.delay(passed_lesson.pk)
        else:
            return Response(data='Incorrect desired status',
                            status=status.HTTP_400_BAD_REQUEST)
        passed_lesson.status = desired_status
        passed_lesson.save()
        return Response()


class LessonView(viewsets.ModelViewSet):
    queryset = Lesson.objects.all().select_related('course', 'ansible_template')
    serializer_class = LessonSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.course.teacher),)
    filterset_fields = ['course']


class PlaybookConstructorView(viewsets.GenericViewSet):

    def get_generated_file(self, request, template_path):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)

        template_file = loader.get_template(template_path)
        response = HttpResponse(content_type='application/x-yaml')
        response['Content-Disposition'] = f'attachment; filename={os.path.basename(template_path)}'
        response.write(template_file.render(serializer.data))

        return response

    @action(methods=['POST'], detail=False, serializer_class=VariablesConstructorSerializer)
    def variables(self, request):
        return self.get_generated_file(request, os.path.join('lms', 'variables_template.yml'))

    @action(methods=['POST'], detail=False, serializer_class=VariablesConstructorSerializer)
    def roles_manipulation(self, request):
        return self.get_generated_file(request, os.path.join('lms', 'roles_manipulation.yml'))

    @action(methods=['POST'], detail=False, serializer_class=RolesListConstructorSerializer)
    def roles_requirements(self, request):
        return self.get_generated_file(request, os.path.join('lms', 'roles_requirements.yml'))


class RolesArchivesView(viewsets.ModelViewSet):
    queryset = RoleArchive.objects.all()
    serializer_class = RolesArchivesSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.creator),)
