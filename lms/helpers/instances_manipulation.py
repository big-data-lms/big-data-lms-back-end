import os

from lms.helpers.other_helpers import random_string
from lms.helpers.instances_manipulation_helpers import check_and_revoke_notification_and_stop_tasks, \
    handle_subprocess_for_manipulating_with_existing_instances, check_and_revoke_terminate_task, \
    handle_subprocess_with_predefined_params, prepare_ansible_execution, get_configure_instances_cmd, \
    get_roles_install_cmd, get_provision_instances_cmd
from lms.models import PassedLesson
from notifications.tasks import notify_student_about_instances


def raise_error_decorator(func):
    def wrapper(*args, **kwargs):
        return_code = func(*args, **kwargs)
        if return_code != 0:
            raise ProvisioningError()
        return return_code

    return wrapper


class ProvisioningError(RuntimeError):
    pass


class InstancesManipulation:

    def __init__(self, passed_lesson_id: int):
        self.passed_lesson = PassedLesson.objects.get(id=passed_lesson_id)
        self.environment_vars = os.environ

    def provision_and_config(self):
        try:
            self.__provision_instances()
            self.__install_roles()
            self.__configure_instances()
            self.__schedule_tasks()
        except ProvisioningError:
            print('An error occurred during executing Ansible')

    def notify_student(self):
        from lms.tasks import stop_task

        if self.passed_lesson.status == PassedLesson.PassedLessonStatus.RUNNING:
            notify_student_about_instances(self.passed_lesson.id)
            stop_task_async_result = stop_task.apply_async(
                (self.passed_lesson.id,),
                countdown=int(self.passed_lesson.lesson.stop_instances_timer.total_seconds())
            )
            self.passed_lesson.is_notification_active = True
            self.passed_lesson.stop_task_id = stop_task_async_result.id
        self.passed_lesson.notification_task_id = None
        self.passed_lesson.save()

    def run_instances(self):
        from lms.tasks import notification_task

        check_and_revoke_notification_and_stop_tasks(self.passed_lesson)
        self.passed_lesson.save()

        handle_subprocess_for_manipulating_with_existing_instances(self.passed_lesson, desired_state='running')
        self.passed_lesson.status = PassedLesson.PassedLessonStatus.RUNNING

        notify_task_async_result = notification_task.apply_async(
            (self.passed_lesson.id,),
            countdown=int(self.passed_lesson.lesson.notification_timer.total_seconds())
        )
        self.passed_lesson.notification_task_id = notify_task_async_result.id
        self.passed_lesson.save()

    def stop_instances(self):
        from lms.tasks import stop_task

        check_and_revoke_notification_and_stop_tasks(self.passed_lesson)
        self.passed_lesson.save()

        handle_subprocess_for_manipulating_with_existing_instances(self.passed_lesson, desired_state='stopped')
        self.passed_lesson.status = PassedLesson.PassedLessonStatus.STOPPED
        stop_task_async_result = stop_task.apply_async((self.passed_lesson.id,), countdown=60 * 60 * 24 * 7 + 60 * 60)
        self.passed_lesson.stop_task_id = stop_task_async_result.id
        self.passed_lesson.is_notification_active = False

        self.passed_lesson.save()

    def terminate_instances(self, after_failed=False):
        check_and_revoke_notification_and_stop_tasks(self.passed_lesson)
        check_and_revoke_terminate_task(self.passed_lesson)
        self.passed_lesson.save()

        handle_subprocess_for_manipulating_with_existing_instances(self.passed_lesson, desired_state='absent')
        self.passed_lesson.status = PassedLesson.PassedLessonStatus.FAILED if after_failed else \
            PassedLesson.PassedLessonStatus.TERMINATED

        self.passed_lesson.save()

    @raise_error_decorator
    def __provision_instances(self):
        prepare_ansible_execution(self.passed_lesson)

        provisioning_return_code, provisioning_logs = handle_subprocess_with_predefined_params(
            cmd=get_provision_instances_cmd(
                playbook_path=self.passed_lesson.ansible_instances_provisioning_playbook_file_path,
                key_path=self.passed_lesson.key_file.path,
                key_name=self.passed_lesson.key_file.name,
                variables_file_path=self.passed_lesson.ansible_variables_file_path
            )
        )

        self.passed_lesson.provisioning_logs = provisioning_logs
        self.passed_lesson.provisioning_return_code = provisioning_return_code
        self.passed_lesson.save()
        return provisioning_return_code

    @raise_error_decorator
    def __install_roles(self):
        roles_path = os.path.join(os.sep, 'tmp', random_string(6))
        while os.path.exists(roles_path):
            roles_path = os.path.join(os.sep, 'tmp', random_string(6))
        os.makedirs(roles_path)

        prepare_ansible_execution(self.passed_lesson)

        return_code, logs = handle_subprocess_with_predefined_params(
            cmd=get_roles_install_cmd(
                requirements_file_path=self.passed_lesson.ansible_roles_requirements_file_path,
                roles_path=roles_path
            )
        )

        self.environment_vars['ANSIBLE_ROLES_PATH'] = roles_path

        return return_code

    @raise_error_decorator
    def __configure_instances(self):
        prepare_ansible_execution(self.passed_lesson)

        config_return_code, config_logs = handle_subprocess_with_predefined_params(
            cmd=get_configure_instances_cmd(
                playbook_path=self.passed_lesson.ansible_role_management_playbook_file_path,
                key_path=self.passed_lesson.key_file.path,
                variables_file_path=self.passed_lesson.ansible_variables_file_path
            ),
            env=self.environment_vars
        )
        self.passed_lesson.configuration_logs = config_logs
        self.passed_lesson.configuration_return_code = config_return_code

        return config_return_code

    def __schedule_tasks(self):
        from lms.tasks import notification_task, terminate_task

        self.passed_lesson.status = PassedLesson.PassedLessonStatus.RUNNING
        notify_task_id = notification_task.apply_async(
            (self.passed_lesson.id,),
            countdown=int(self.passed_lesson.lesson.notification_timer.total_seconds())
        )
        terminate_task_id = terminate_task.apply_async(
            (self.passed_lesson.id,),
            countdown=int(self.passed_lesson.lesson.terminate_instances_timer.total_seconds())
        )
        self.passed_lesson.notification_task_id = notify_task_id
        self.passed_lesson.terminate_task_id = terminate_task_id
        self.passed_lesson.save()
