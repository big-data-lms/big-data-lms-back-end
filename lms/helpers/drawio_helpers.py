import base64
import json
import os
import zlib
from urllib.parse import quote, unquote
from dataclasses import dataclass

from bs4 import BeautifulSoup
from django.core.files.base import ContentFile
from django.template import loader


@dataclass
class Node:
    id: str
    value: str
    parent_id: str
    children: list


@dataclass
class SetOfInstances:
    count_tag_name: str
    count_tag_value: str
    count: int
    type: str
    all_tags: list


def transform_tree_to_target_list_with_parents(root, instance):
    result = []
    random_uuid = instance.lesson.ansible_template.template_uuid

    def helper(node, prev_items):
        if len(node.children[0].children) == 0:
            count_tag_value, node_type = tuple(node.value.split(';', maxsplit=1))
            all_tags = [
                {'name': f'{random_uuid}__{item.value}', 'value': item.value}
                for item in prev_items if item.id != '1'
            ]
            all_tags.append({'name': f'{random_uuid}__{count_tag_value}', 'value': count_tag_value})
            new_set_of_instances = SetOfInstances(
                count_tag_name=f'{random_uuid}__{count_tag_value}',
                count_tag_value=count_tag_value,
                count=len(node.children),
                type=node_type,
                all_tags=all_tags
            )
            result.append(new_set_of_instances)
        else:
            for child in node.children:
                helper(child, prev_items + [node])

    helper(root, [])
    return [vars(r) for r in result]


def construct_tree(root_tag):
    root_element = Node(id='1', value='', parent_id='', children=[])
    dictionary = {'1': root_element}

    for element in root_tag.find_all('mxCell', id=lambda x: x not in ('0', '1')):
        dictionary[element['id']] = Node(
            id=element['id'],
            value=element['value'],
            parent_id=element['parent'],
            children=[]
        )

    for key, val in dictionary.items():
        if val.parent_id != '':
            dictionary[val.parent_id].children.append(val)

    return root_element


def create_list_with_parents_from_drawio_file(drawio_file_content, instance):
    soup = BeautifulSoup(drawio_file_content, 'lxml-xml')
    root_tag = soup.find('root')
    root_element = construct_tree(root_tag)
    list_with_parents = transform_tree_to_target_list_with_parents(root_element, instance)
    return list_with_parents


def write_list_with_parents_to_variables_file(instance, list_with_parents):
    template_file = loader.get_template(os.path.join('lms', 'variables_template.yml'))
    rendered_template = template_file.render({'sets': list_with_parents})
    instance.variables_file.save('variables.yaml', ContentFile(rendered_template))


def transform_drawio_to_variables_file_if_exists(instance, drawio_file_content):
    list_with_parents = create_list_with_parents_from_drawio_file(drawio_file_content, instance)
    write_list_with_parents_to_variables_file(instance, list_with_parents)


def deflate_and_base64_encode(string_val):
    zlibbed_str = zlib.compress(quote(string_val).encode('utf-8'))
    compressed_string = zlibbed_str[2:-4]
    return base64.b64encode(compressed_string).decode('utf-8')


def decode_base64_and_inflate(b64string):
    decoded_data = base64.b64decode(b64string)
    return unquote(zlib.decompress(decoded_data, -15).decode('utf-8'))


def modify_drawio_library_file_if_exists(instance):
    if instance.drawio_library_file:
        with open(instance.drawio_library_file.path, 'r+') as drawio_file:
            drawio_file_file_content = drawio_file.read()
            # Slice because file begins and ends with xml tags
            parsed_json_file_content = json.loads(drawio_file_file_content[11:-12])
            for obj in parsed_json_file_content:
                if 'title' in obj:
                    decoded_element = decode_base64_and_inflate(obj['xml'])
                    soup = BeautifulSoup(decoded_element, 'lxml-xml')

                    target_item = soup.find(id='2')
                    target_item['value'] = obj['title']
                    del target_item['id']

                    updated_encoded_element = deflate_and_base64_encode(str(soup).split("\n")[-1])
                    obj['xml'] = updated_encoded_element
            drawio_file.seek(0)
            drawio_file.write(f'<mxlibrary>{json.dumps(parsed_json_file_content)}</mxlibrary>')
            drawio_file.truncate()
