import os
import subprocess
from subprocess import Popen

from big_data_lms.celery import app
from big_data_lms.settings import ANSIBLE_FILES_DIR


def get_roles_install_cmd(requirements_file_path, roles_path):
    arguments = locals()
    roles_install_cmd = 'ansible-galaxy install -r {requirements_file_path} --roles-path {roles_path}'
    return roles_install_cmd.format(**arguments)


def get_provision_instances_cmd(playbook_path, key_path, key_name, variables_file_path):
    arguments = locals()
    provision_instances_cmd = 'ansible-playbook {playbook_path} \
                               --extra-vars "private_key_path={key_path} \
                               private_key_name={key_name} \
                               variables_file_path={variables_file_path}" \
                               --private-key {key_path}'
    return provision_instances_cmd.format(**arguments)


def get_existing_instances_manipulation_cmd(state, variables_file_path):
    arguments = locals()
    existing_instances_manipulation_cmd = 'ansible-playbook {playbook_path} \
                                           --extra-vars "target_state={state} \
                                           variables_file_path={variables_file_path}"'
    return existing_instances_manipulation_cmd.format(
        playbook_path=os.path.join(ANSIBLE_FILES_DIR, 'amazon_existing_instances_manipulation.yml'),
        **arguments
    )


def get_configure_instances_cmd(playbook_path, variables_file_path, key_path):
    arguments = locals()
    configure_instances_cmd = 'ansible-playbook {playbook_path} \
                               --extra-vars "variables_file_path={variables_file_path}" \
                               -i ec2.py \
                               --private-key {key_path}'
    return configure_instances_cmd.format(**arguments)


def handle_subprocess(*args, **kwargs):
    subprocess_output = ''
    with Popen(*args, **kwargs) as p:
        for line in p.stdout:
            line = line.decode()
            subprocess_output += line
            print(line, end='')
    return p.returncode, subprocess_output


def handle_subprocess_with_predefined_params(cmd, env=os.environ):
    return handle_subprocess(
        cmd,
        shell=True,
        cwd=ANSIBLE_FILES_DIR,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        env=env
    )


def handle_subprocess_for_manipulating_with_existing_instances(passed_lesson, desired_state):
    prepare_ansible_execution(passed_lesson)

    return handle_subprocess_with_predefined_params(
        get_existing_instances_manipulation_cmd(
            state=desired_state,
            variables_file_path=passed_lesson.ansible_variables_file_path
        )
    )


def prepare_ansible_execution(passed_lesson):
    os.environ['EC2_INI_PATH'] = os.path.join(ANSIBLE_FILES_DIR, 'ec2.ini')
    os.environ['AWS_ACCESS_KEY_ID'] = passed_lesson.student.aws_access_key_id
    os.environ['AWS_SECRET_ACCESS_KEY'] = passed_lesson.student.aws_secret_access_key
    os.environ['AWS_SESSION_TOKEN'] = passed_lesson.student.aws_session_token
    os.environ['ANSIBLE_HOST_KEY_CHECKING'] = 'False'


def check_and_revoke_notification_and_stop_tasks(passed_lesson):
    if passed_lesson.stop_task_id:
        app.control.revoke(passed_lesson.stop_task_id)
        passed_lesson.stop_task_id = None
    if passed_lesson.notification_task_id:
        app.control.revoke(passed_lesson.notification_task_id)
        passed_lesson.notification_task_id = None


def check_and_revoke_terminate_task(passed_lesson):
    if passed_lesson.terminate_task_id:
        app.control.revoke(passed_lesson.terminate_task_id)
        passed_lesson.terminate_task_id = None
