import base64
import random
import string
import zlib
from urllib.parse import quote, unquote


def random_string_digits(string_length=6):
    letters_and_digits = string.ascii_letters + string.digits
    return ''.join(random.choice(letters_and_digits) for _ in range(string_length))


def random_string(string_length=6):
    return ''.join(random.choice(string.ascii_letters) for _ in range(string_length))


def key_upload_path(instance, filename):
    return 'private_keys/{0}/{1}'.format(instance.student.pk, filename)
