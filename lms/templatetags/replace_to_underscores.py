from django import template


register = template.Library()


@register.filter
def replace_to_underscores(value):
    return value.replace('-', '_')
