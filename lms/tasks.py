from big_data_lms.celery import app
from lms.helpers.instances_manipulation import InstancesManipulation


@app.task
def provision_and_configure_instances(passed_lesson_id):
    InstancesManipulation(passed_lesson_id).provision_and_config()


@app.task
def notification_task(passed_lesson_id):
    InstancesManipulation(passed_lesson_id).notify_student()


@app.task
def run_task(passed_lesson_id):
    InstancesManipulation(passed_lesson_id).run_instances()


@app.task
def stop_task(passed_lesson_id):
    InstancesManipulation(passed_lesson_id).stop_instances()


@app.task
def terminate_task(passed_lesson_id, after_failed=False):
    InstancesManipulation(passed_lesson_id).terminate_instances(after_failed=after_failed)
