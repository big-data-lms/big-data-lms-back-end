from rest_framework import routers
from lms.views import CourseView, LessonView, PassedLessonView, PlaybookConstructorView, RolesArchivesView

router = routers.DefaultRouter()
router.register('courses', CourseView)
router.register('lessons', LessonView)
router.register('passed_lessons', PassedLessonView)
router.register('playbooks_constructor', PlaybookConstructorView,
                basename='Constructor of playbooks')
router.register('roles_archives', RolesArchivesView)
