from django.db.models.signals import post_save
from django.dispatch import receiver

from lms.helpers.drawio_helpers import modify_drawio_library_file_if_exists
from lms.models import PassedLesson, AnsibleTemplate


@receiver(post_save, sender=AnsibleTemplate)
def ansible_template_post_save(sender, instance, created, *args, **kwargs):
    if created:
        modify_drawio_library_file_if_exists(instance)
