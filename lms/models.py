import os
import uuid

from django.contrib.auth.models import AbstractUser
from django.core.files.base import ContentFile
from django.db import models
from django.utils.translation import gettext_lazy as _

from accounts.models import BasicUser
from big_data_lms.settings import ANSIBLE_FILES_DIR
from lms.helpers.other_helpers import key_upload_path, random_string_digits


class Course(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField()
    teacher = models.ForeignKey(BasicUser, on_delete=models.CASCADE, related_name='my_courses')
    notification_timer = models.DurationField()
    stop_instances_timer = models.DurationField()
    terminate_instances_timer = models.DurationField()

    def __str__(self):
        return self.name


class Lesson(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField()
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name='lessons')
    ansible_template = models.ForeignKey('AnsibleTemplate', on_delete=models.CASCADE, related_name='lessons')
    _notification_timer = models.DurationField(null=True, blank=True)
    _stop_instances_timer = models.DurationField(null=True, blank=True)
    _terminate_instances_timer = models.DurationField(null=True, blank=True)

    @property
    def notification_timer(self):
        return self._notification_timer if self._notification_timer else self.course.notification_timer

    @property
    def stop_instances_timer(self):
        return self._stop_instances_timer if self._stop_instances_timer else self.course.stop_instances_timer

    @property
    def terminate_instances_timer(self):
        return self._terminate_instances_timer if self._terminate_instances_timer else \
            self.course.terminate_instances_timer

    @notification_timer.setter
    def notification_timer(self, value):
        self._notification_timer = value

    @stop_instances_timer.setter
    def stop_instances_timer(self, value):
        self._stop_instances_timer = value

    @terminate_instances_timer.setter
    def terminate_instances_timer(self, value):
        self._terminate_instances_timer = value

    def __str__(self):
        return self.name


class PassedLesson(models.Model):
    class PassedLessonStatus(models.TextChoices):
        CREATING = 'cr', _('Creating')
        RUNNING = 'ru', _('Running')
        FAILED = 'fa', _('Failed')
        STOPPED = 'st', _('Stopped')
        TERMINATED = 'te', _('Terminated')

    student = models.ForeignKey(BasicUser, on_delete=models.CASCADE)
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    start_datetime = models.DateTimeField(auto_now=True)
    provisioning_return_code = models.IntegerField(default=-1)
    configuration_return_code = models.IntegerField(default=-1)
    provisioning_logs = models.TextField(default='')
    configuration_logs = models.TextField(default='')
    key_file = models.FileField(
        upload_to=key_upload_path,
        default=ContentFile('', name=f'{random_string_digits(10)}.pem')
    )
    status = models.CharField(
        max_length=2,
        choices=PassedLessonStatus.choices,
        default=PassedLessonStatus.CREATING,
    )
    is_notification_active = models.BooleanField(default=False)
    notification_task_id = models.CharField(max_length=36, null=True, blank=True)
    stop_task_id = models.CharField(max_length=36, null=True, blank=True)
    terminate_task_id = models.CharField(max_length=36, null=True, blank=True)
    variables_file = models.FileField(upload_to='ansible_variables', null=True, blank=True)

    @property
    def ansible_variables_file_path(self):
        return self.variables_file.path if self.variables_file else self.lesson.ansible_template.variables_file.path

    @property
    def ansible_roles_requirements_file_path(self):
        return self.lesson.ansible_template.roles_requirements_file.path

    @property
    def ansible_role_management_playbook_file_path(self):
        return self.lesson.ansible_template.role_management_playbook_file.path

    @property
    def ansible_instances_provisioning_playbook_file_path(self):
        return self.lesson.ansible_template.instances_provisioning_playbook_file_path


class AnsibleTemplate(models.Model):
    description = models.TextField(null=True, blank=True)
    roles_requirements_file = models.FileField(upload_to='ansible_roles_requirements')
    role_management_playbook_file = models.FileField(upload_to='ansible_management_playbooks')
    variables_file = models.FileField(upload_to='ansible_variables')
    instances_provisioning_playbook_file = models.FileField(
        null=True,
        blank=True,
        upload_to='ansible_instances_management'
    )
    drawio_library_file = models.FileField(
        null=True,
        blank=True,
        upload_to='drawio_files'
    )
    template_uuid = models.UUIDField(default=uuid.uuid4())

    @property
    def instances_provisioning_playbook_file_path(self):
        if self.instances_provisioning_playbook_file:
            return self.instances_provisioning_playbook_file.path
        return os.path.join(ANSIBLE_FILES_DIR, 'amazon_instances_provisioning.yml')


class RoleArchive(models.Model):
    creator = models.ForeignKey(BasicUser, related_name='roles_archives', on_delete=models.CASCADE)
    role_archive_file = models.FileField(upload_to='ansible_roles_archive')
