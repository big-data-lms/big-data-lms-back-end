from django.contrib import admin

from lms.models import Course, Lesson, PassedLesson, AnsibleTemplate, RoleArchive

admin.site.register(Course)
admin.site.register(Lesson)
admin.site.register(PassedLesson)
admin.site.register(AnsibleTemplate)
admin.site.register(RoleArchive)
