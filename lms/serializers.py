from drf_writable_nested import WritableNestedModelSerializer
from rest_framework import serializers

from accounts.serializers import UserShowSerializer
from lms.helpers.drawio_helpers import transform_drawio_to_variables_file_if_exists
from lms.models import AnsibleTemplate, Lesson, PassedLesson, Course, RoleArchive
from lms.tasks import provision_and_configure_instances


class AnsibleTemplateSerializer(WritableNestedModelSerializer):
    class Meta:
        model = AnsibleTemplate
        fields = (
            'id', 'roles_requirements_file', 'role_management_playbook_file',
            'instances_provisioning_playbook_file', 'variables_file', 'drawio_library_file', 'template_uuid'
        )
        extra_kwargs = {
            'instances_provisioning_playbook_file': {'required': False},
            'drawio_library_file': {'required': False},
            'template_uuid': {'write_only': True}
        }


class LessonSerializer(WritableNestedModelSerializer):
    ansible_template = AnsibleTemplateSerializer()
    notification_timer = serializers.DurationField()
    stop_instances_timer = serializers.DurationField()
    terminate_instances_timer = serializers.DurationField()

    class Meta:
        model = Lesson
        fields = (
            'id', 'name', 'description', 'course',
            'ansible_template', 'notification_timer', 'stop_instances_timer', 'terminate_instances_timer'
        )


class LessonInsidePassedLessonSerializer(WritableNestedModelSerializer):
    class Meta:
        model = Lesson
        fields = ('id', 'name')


class PassedLessonSerializer(serializers.ModelSerializer):
    student = serializers.HiddenField(default=serializers.CurrentUserDefault(), write_only=True)
    student_id = serializers.PrimaryKeyRelatedField(source='student', read_only=True)
    lesson = LessonInsidePassedLessonSerializer(read_only=True)
    lesson_id = serializers.PrimaryKeyRelatedField(source='lesson', queryset=Lesson.objects.all(), write_only=True)
    drawio_file = serializers.FileField(write_only=True, required=False)

    class Meta:
        model = PassedLesson
        fields = (
            'id', 'student', 'student_id', 'lesson', 'lesson_id', 'start_datetime', 'provisioning_return_code',
            'configuration_return_code', 'provisioning_logs', 'configuration_logs', 'key_file', 'status',
            'is_notification_active', 'drawio_file'
        )
        read_only_fields = ('id', 'provisioning_logs', 'configuration_logs', 'provisioning_return_code',
                            'configuration_return_code', 'key_file', 'status', 'is_notification_active')
        extra_kwargs = {
            'drawio_file': {'required': False}
        }

    def create(self, validated_data):
        drawio_file = validated_data.pop('drawio_file', None)
        instance = super().create(validated_data)
        if drawio_file:
            file_content = drawio_file.read().decode('utf-8')
            transform_drawio_to_variables_file_if_exists(instance, file_content)
        provision_and_configure_instances.delay(instance.pk)
        return instance


class PassedLessonEmptySerializer(serializers.ModelSerializer):
    class Meta:
        model = PassedLesson
        fields = ()


class PassedLessonStatusSerializer(serializers.ModelSerializer):
    status = serializers.ChoiceField(choices=[(k, v) for (k, v) in PassedLesson.PassedLessonStatus.choices
                                              if k in [e.value for e in
                                                       [PassedLesson.PassedLessonStatus.RUNNING,
                                                        PassedLesson.PassedLessonStatus.STOPPED,
                                                        PassedLesson.PassedLessonStatus.TERMINATED]]])

    class Meta:
        model = PassedLesson
        fields = ('status',)


class CourseSerializer(serializers.ModelSerializer):
    teacher = UserShowSerializer(read_only=True)
    teacher_id = serializers.HiddenField(source='teacher', write_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = Course
        fields = (
            'id', 'name', 'description', 'teacher', 'teacher_id',
            'notification_timer', 'stop_instances_timer', 'terminate_instances_timer'
        )


class AnotherTagsSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256)
    value = serializers.CharField(max_length=256)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class SetOfInstancesConstructorInfoSerializer(serializers.Serializer):
    count_tag_name = serializers.CharField(max_length=256)
    count_tag_value = serializers.CharField(max_length=256)
    count = serializers.IntegerField(min_value=1)
    type = serializers.CharField(max_length=20)
    all_tags = serializers.ListField(
        child=AnotherTagsSerializer()
    )

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class VariablesConstructorSerializer(serializers.Serializer):
    sets = serializers.ListField(
        child=SetOfInstancesConstructorInfoSerializer()
    )

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class SpecificRoleConstructorSerializer(serializers.Serializer):
    name = serializers.CharField(max_length=256)
    src = serializers.CharField(max_length=256, allow_blank=True, allow_null=True)
    version = serializers.CharField(max_length=256, allow_blank=True, allow_null=True)
    scm = serializers.CharField(max_length=256, allow_blank=True, allow_null=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class RolesListConstructorSerializer(serializers.Serializer):
    roles = serializers.ListField(
        child=SpecificRoleConstructorSerializer()
    )

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass


class RolesArchivesSerializer(serializers.ModelSerializer):
    creator = serializers.HiddenField(write_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = RoleArchive
        fields = ('id', 'role_archive_file', 'creator')
