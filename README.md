# Big Data LMS (Back-end)

Самый простой способ локального запуска проекта - использовать Docker-compose.  
Не рекомендуется использовать текущие версии docker-файлов production-окружения.

### Команды

* `docker-compose up` - запустить проект локально
* `docker-compose up -d` - запустить в background режиме
* `docker-compose logs` - показать логи из контейнеров (полезно, если вы запускали в background)
* `docker-compose down` - остановить и удалить все контейнеры
* `docker-compose exec django-backend ...` - выполнять команды внутри контейнера с django (см. ниже)
* `docker-compose exec django-backend python manage.py ...` - выполнять команды manage.py
