from django.core.mail import EmailMessage


def get_email_obj_for_notify_student(passed_lesson, connection):
    return EmailMessage(
        subject='Notification - instances will be stopped',
        body=f'Read notification for id {passed_lesson.id}',
        to=[passed_lesson.student.email],
        connection=connection
    )
