from django.core import mail

from big_data_lms.celery import app
from lms.models import PassedLesson
from notifications.email_messages import get_email_obj_for_notify_student


def notify_student_about_instances(passed_lesson_id):
    send_email_for_notify_student_about_instances.delay(passed_lesson_id)


@app.task
def send_email_for_notify_student_about_instances(passed_lesson_id):
    passed_lesson = PassedLesson.objects.get(id=passed_lesson_id)
    with mail.get_connection() as connection:
        get_email_obj_for_notify_student(passed_lesson, connection).send()
