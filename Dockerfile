FROM python:3.8.3-alpine3.12

ENV PYTHONUNBUFFERED 1

EXPOSE 8000

WORKDIR /app

RUN apk update && apk add gcc libc-dev make git libffi-dev openssl-dev python3-dev libxml2-dev libxslt-dev postgresql-libs postgresql-dev

ADD requirements.txt .

RUN pip install -r requirements.txt

COPY . .

ENTRYPOINT python manage.py migrate && gunicorn -b 0.0.0.0:8000 \
                                -w 4 --threads 4 \
                                --worker-connections 500 \
                                --max-requests 1000 big_data_lms.wsgi
