from django.db.transaction import atomic
from rest_framework import serializers

from accounts.serializers import UserShowSerializer
from lms.models import Course
from lms.serializers import CourseSerializer
from personalized_learning.models import LearningPath, LearningPathDependency, AdaptiveTest, Question, AnswerOption, \
    TestPassing, FavoriteLearningPath


class LearningPathSimpleSerializer(serializers.ModelSerializer):
    class Meta:
        model = LearningPath
        fields = ('id', 'name')


class LearningPathDependencySerializer(serializers.ModelSerializer):
    course = CourseSerializer(read_only=True)
    course_id = serializers.PrimaryKeyRelatedField(
        source='course',
        write_only=True,
        queryset=Course.objects.all()
    )

    class Meta:
        model = LearningPathDependency
        fields = ('id', 'learning_path', 'course', 'course_id', 'serial_number')


class LearningPathDependencyCreateListSerializer(serializers.Serializer):
    learning_path_dependencies = LearningPathDependencySerializer(many=True)

    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        result = []
        for dependency in validated_data['learning_path_dependencies']:
            result.append(LearningPathDependency.objects.create(**dependency))
        return result


class LearningPathSerializer(serializers.ModelSerializer):
    creator = UserShowSerializer(read_only=True)
    creator_id = serializers.HiddenField(source='creator', write_only=True, default=serializers.CurrentUserDefault())
    favorite_learning_paths = serializers.PrimaryKeyRelatedField(many=True, read_only=True)

    class Meta:
        model = LearningPath
        fields = ('id', 'name', 'description', 'creator', 'creator_id', 'favorite_learning_paths')


class FavoriteLearningPathSerializer(serializers.ModelSerializer):
    user = serializers.HiddenField(write_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = FavoriteLearningPath
        fields = ('id', 'learning_path', 'user')


class AnswerOptionSerializer(serializers.ModelSerializer):
    learning_path_id = serializers.PrimaryKeyRelatedField(
        source='learning_path',
        write_only=True,
        queryset=LearningPath.objects.all(),
        allow_null=True
    )
    next_question = serializers.PrimaryKeyRelatedField(allow_null=True, queryset=Question.objects.all())
    learning_path = LearningPathSimpleSerializer(read_only=True)

    class Meta:
        model = AnswerOption
        fields = ('id', 'text', 'question', 'next_question', 'learning_path', 'learning_path_id')

    # TODO Check for loops in graph
    # def validate(self, attrs):
    #     return super(AnswerOptionSerializer, self).validate(attrs)


class AnswerOptionStudentResponse(serializers.ModelSerializer):
    answer_options = serializers.PrimaryKeyRelatedField(queryset=AnswerOption.objects.all())

    class Meta:
        model = TestPassing
        fields = ('answer_options',)


class QuestionSerializer(serializers.ModelSerializer):
    adaptive_test = serializers.PrimaryKeyRelatedField(write_only=True, queryset=AdaptiveTest.objects.all())
    answer_options = AnswerOptionSerializer(many=True, read_only=True)

    class Meta:
        model = Question
        fields = ('id', 'text', 'adaptive_test', 'answer_options')


class AdaptiveTestSerializer(serializers.ModelSerializer):
    creator = UserShowSerializer(read_only=True)
    creator_id = serializers.HiddenField(source='creator', write_only=True, default=serializers.CurrentUserDefault())
    first_question = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = AdaptiveTest
        fields = ('id', 'name', 'description', 'creator', 'creator_id', 'first_question')

    @atomic
    def create(self, validated_data):
        created_test = super().create(validated_data)
        created_question = Question.objects.create(text='Первый вопрос', adaptive_test=created_test)
        created_test.first_question = created_question
        created_test.save()
        return created_test


class TestPassingSerializer(serializers.ModelSerializer):
    test = AdaptiveTestSerializer(read_only=True)
    test_id = serializers.PrimaryKeyRelatedField(source='test', write_only=True, queryset=AdaptiveTest.objects.all())
    user = serializers.HiddenField(write_only=True, default=serializers.CurrentUserDefault())

    class Meta:
        model = TestPassing
        fields = ('id', 'test', 'test_id', 'current_question', 'user', 'learning_path')
        read_only_fields = ('current_question', 'learning_path')

    def create(self, validated_data):
        test = validated_data['test']
        validated_data['current_question'] = test.first_question
        return super().create(validated_data)
