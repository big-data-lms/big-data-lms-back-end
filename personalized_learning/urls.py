from rest_framework import routers
from personalized_learning.views import LearningPathView, LearningPathDependencyView, AdaptiveTestView, QuestionView, \
    AnswerOptionView, TestPassingView, FavoriteLearningPathView

router = routers.DefaultRouter()
router.register('learning_paths', LearningPathView, basename='learning_paths')
router.register('learning_path_dependencies', LearningPathDependencyView)
router.register('favorite_learning_paths', FavoriteLearningPathView, basename='favorite_learning_paths')
router.register('adaptive_tests', AdaptiveTestView)
router.register('questions', QuestionView)
router.register('answer_options', AnswerOptionView)
# router.register('target_courses', TargetCourseView)
router.register('test_passings', TestPassingView, basename='test_passings')
