from django.apps import AppConfig


class PersonalizedLearningConfig(AppConfig):
    name = 'personalized_learning'
