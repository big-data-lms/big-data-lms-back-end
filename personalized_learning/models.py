from django.db import models

from accounts.models import BasicUser
from lms.models import Course


class LearningPath(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField(null=True, blank=True)
    creator = models.ForeignKey(BasicUser, related_name='learning_paths', on_delete=models.CASCADE)
    student = models.ForeignKey(
        BasicUser,
        related_name='personal_learning_paths',
        null=True,
        blank=True,
        on_delete=models.CASCADE
    )


class LearningPathDependency(models.Model):
    learning_path = models.ForeignKey(LearningPath, related_name='learning_path_dependencies', on_delete=models.CASCADE)
    course = models.ForeignKey(Course, related_name='learning_path_dependencies', on_delete=models.CASCADE)
    serial_number = models.PositiveSmallIntegerField()

    class Meta:
        unique_together = ('learning_path', 'serial_number')


class FavoriteLearningPath(models.Model):
    learning_path = models.ForeignKey(LearningPath, related_name='favorite_learning_paths', on_delete=models.CASCADE)
    user = models.ForeignKey(BasicUser, related_name='favorite_learning_paths', on_delete=models.CASCADE)


class AdaptiveTest(models.Model):
    name = models.CharField(max_length=80)
    description = models.TextField(null=True, blank=True)
    creator = models.ForeignKey(BasicUser, related_name='adaptive_tests', on_delete=models.CASCADE)
    first_question = models.OneToOneField(
        'Question',
        related_name='adaptive_test_as_first_question',
        on_delete=models.CASCADE,
        null=True,
        blank=True
    )


class Question(models.Model):
    text = models.TextField()
    adaptive_test = models.ForeignKey(AdaptiveTest, related_name='questions', on_delete=models.CASCADE)


class AnswerOption(models.Model):
    text = models.TextField()
    question = models.ForeignKey(Question, related_name='answer_options', on_delete=models.CASCADE)
    next_question = models.ForeignKey(
        Question,
        related_name='previous_options',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )
    learning_path = models.ForeignKey(
        LearningPath,
        related_name='answer_options',
        null=True,
        blank=True,
        on_delete=models.SET_NULL
    )


class TestPassing(models.Model):
    test = models.ForeignKey(AdaptiveTest, related_name='test_passings', on_delete=models.CASCADE)
    learning_path = models.OneToOneField(
        LearningPath,
        null=True,
        blank=True,
        related_name='test_passings',
        on_delete=models.CASCADE
    )
    current_question = models.ForeignKey(
        Question,
        null=True,
        blank=True,
        related_name='test_passings',
        on_delete=models.CASCADE
    )
    user = models.ForeignKey(BasicUser, related_name='test_passings', on_delete=models.CASCADE)


class TestPassingLearningPath(models.Model):
    test_passing = models.ForeignKey(
        TestPassing,
        related_name='test_passing_learning_paths',
        on_delete=models.CASCADE
    )
    learning_path = models.ForeignKey(
        LearningPath,
        related_name='test_passing_learning_paths',
        on_delete=models.CASCADE
    )
