from django.contrib import admin

from personalized_learning.models import LearningPath, LearningPathDependency, FavoriteLearningPath, AdaptiveTest, \
    Question, AnswerOption, TestPassing, TestPassingLearningPath

admin.site.register(LearningPath)
admin.site.register(LearningPathDependency)
admin.site.register(FavoriteLearningPath)
admin.site.register(AdaptiveTest)
admin.site.register(Question)
admin.site.register(AnswerOption)
admin.site.register(TestPassing)
admin.site.register(TestPassingLearningPath)
