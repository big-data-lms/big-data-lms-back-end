import datetime

from django.db.models import Prefetch, Q
from django.db.transaction import atomic
from rest_framework import viewsets
from rest_framework.response import Response
from rest_framework.decorators import action

from accounts.permissions import is_owner_or_read_only
from personalized_learning.filters import LearningPathFilter
from personalized_learning.models import LearningPath, LearningPathDependency, AdaptiveTest, TestPassing, \
    AnswerOption, Question, FavoriteLearningPath, TestPassingLearningPath
from personalized_learning.serializers import LearningPathSerializer, LearningPathDependencySerializer, \
    TestPassingSerializer, AnswerOptionSerializer, QuestionSerializer, AdaptiveTestSerializer, \
    FavoriteLearningPathSerializer, LearningPathDependencyCreateListSerializer, AnswerOptionStudentResponse


class LearningPathView(viewsets.ModelViewSet):
    serializer_class = LearningPathSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.creator),)
    filterset_class = LearningPathFilter

    def get_queryset(self):
        current_user = self.request.user
        return LearningPath.objects.all() \
            .order_by('-id') \
            .select_related('creator') \
            .prefetch_related('learning_path_dependencies',
                              Prefetch(
                                  'favorite_learning_paths',
                                  queryset=FavoriteLearningPath.objects.filter(user=current_user)
                              )) \
            .filter(Q(student=None) | Q(student=current_user))

    @action(methods=['PUT'], detail=True, serializer_class=LearningPathDependencyCreateListSerializer)
    def write_dependencies(self, request, pk=None):
        learning_path = self.get_object()

        LearningPathDependency.objects.filter(learning_path=learning_path).delete()

        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response()


class LearningPathDependencyView(viewsets.ModelViewSet):
    queryset = LearningPathDependency.objects.all()
    serializer_class = LearningPathDependencySerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.learning_path.creator),)
    filterset_fields = ('learning_path',)


class FavoriteLearningPathView(viewsets.ModelViewSet):
    serializer_class = FavoriteLearningPathSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.user),)

    def get_queryset(self):
        return FavoriteLearningPath.objects.filter(user=self.request.user)


class AdaptiveTestView(viewsets.ModelViewSet):
    queryset = AdaptiveTest.objects.all().order_by('-id').select_related('creator')
    serializer_class = AdaptiveTestSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.creator),)


class QuestionView(viewsets.ModelViewSet):
    queryset = Question.objects.all().prefetch_related('answer_options', 'answer_options__learning_path')
    serializer_class = QuestionSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.adaptive_test.creator),)
    filterset_fields = ('adaptive_test',)


class AnswerOptionView(viewsets.GenericViewSet,
                       viewsets.mixins.CreateModelMixin,
                       viewsets.mixins.UpdateModelMixin,
                       viewsets.mixins.DestroyModelMixin):
    queryset = AnswerOption.objects.all()
    serializer_class = AnswerOptionSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.question.adaptive_test.creator),)


class TestPassingView(viewsets.ModelViewSet):
    serializer_class = TestPassingSerializer
    permission_classes = (is_owner_or_read_only(lambda obj: obj.user),)

    def get_queryset(self):
        return TestPassing \
            .objects \
            .order_by('-id') \
            .select_related('user') \
            .filter(user=self.request.user)

    @action(methods=['GET'], detail=True)
    def get_current_question(self, request, pk=None):
        test_passing_obj = self.get_object()
        return Response(QuestionSerializer(test_passing_obj.current_question).data)

    @atomic
    @action(methods=['POST'], detail=True, serializer_class=AnswerOptionStudentResponse)
    def write_answer(self, request, pk=None):
        test_passing_obj = self.get_object()
        answer_option_serializer = self.get_serializer(data=request.data)
        answer_option_serializer.is_valid(raise_exception=True)
        answer_option_obj = AnswerOption.objects.get(id=answer_option_serializer.data['answer_options'])

        related_learning_path = answer_option_obj.learning_path
        if related_learning_path:
            TestPassingLearningPath.objects.create(
                learning_path=related_learning_path,
                test_passing=test_passing_obj
            )

        test_passing_obj.current_question = answer_option_obj.next_question
        if test_passing_obj.current_question is None:
            test_passing_learning_paths_queryset = test_passing_obj \
                .test_passing_learning_paths \
                .prefetch_related('learning_path__learning_path_dependencies')
            personal_learning_path = LearningPath.objects.create(
                name=f'Персональная траектория {test_passing_obj.test.name} от {datetime.datetime.now()}',
                description=f'Персональная траектория сформирована в результате прохождения теста "{test_passing_obj.test.name}" от {datetime.datetime.now()}',
                creator=test_passing_obj.test.creator,
                student=self.request.user
            )
            test_passing_obj.learning_path = personal_learning_path

            for i, course_id in enumerate(set(
                test_passing_learning_paths_queryset.values_list('learning_path__learning_path_dependencies__course',
                                                                 flat=True)
            )):
                if course_id is not None:
                    LearningPathDependency.objects.create(
                        learning_path=personal_learning_path,
                        course_id=course_id,
                        serial_number=i
                    )

        test_passing_obj.save()

        return Response(test_passing_obj.current_question.id if test_passing_obj.current_question else None)
