from django_filters import rest_framework as filters

from personalized_learning.models import LearningPath


PUBLIC_PERSONAL_TOGGLE_CHOICES = (
    ('a', 'Все'),
    ('pu', 'Публичные'),
    ('pe', 'Персональные'),
)


class LearningPathFilter(filters.FilterSet):
    public_personal_toggle = filters.ChoiceFilter(
        choices=PUBLIC_PERSONAL_TOGGLE_CHOICES,
        method='public_private_toggle'
    )
    only_my = filters.BooleanFilter(method='get_created_by_user')
    only_favorite = filters.BooleanFilter(method='get_only_favorite')

    def public_private_toggle(self, queryset, name, value):
        if value == 'a':
            return queryset
        elif value == 'pu':
            return queryset.filter(student=None)
        else:
            return queryset.filter(student=self.request.user)

    def get_created_by_user(self, queryset, name, value):
        if value:
            return queryset.filter(creator=self.request.user)
        return queryset

    def get_only_favorite(self, queryset, name, value):
        if value:
            return queryset.filter(favorite_learning_paths__user=self.request.user)
        return queryset

    class Meta:
        model = LearningPath
        fields = ('creator', 'only_my', 'only_favorite', 'public_personal_toggle', 'name')
