from rest_framework.permissions import SAFE_METHODS, IsAuthenticated


def is_owner_or_read_only(get_user_function):
    class IsOwnerOrReadOnly(IsAuthenticated):
        def has_object_permission(self, request, view, obj):
            if request.method not in SAFE_METHODS:
                return super().has_object_permission(request, view, obj) and get_user_function(obj) == request.user
            return super().has_object_permission(request, view, obj)
    return IsOwnerOrReadOnly
