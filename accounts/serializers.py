from rest_framework import serializers
from rest_framework.validators import UniqueValidator
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer

from accounts.models import BasicUser
from accounts.validators import FIOValidator
from accounts.mixins import WriteOnceMixin


class UserSerializer(WriteOnceMixin, serializers.ModelSerializer):
    email = serializers.EmailField(allow_blank=False, required=True, max_length=254,
                                   validators=(UniqueValidator(queryset=BasicUser.objects.all()),))
    fio = serializers.CharField(allow_blank=False, required=True, max_length=100,
                                validators=(FIOValidator(),))

    def validate(self, attrs):
        attrs = super().validate(attrs)
        if 'email' in attrs:
            attrs['username'] = attrs['email']
        if self.instance:  # is update
            # Password should be updated using ChangePasswordSerializer
            attrs.pop('password', None)
        return attrs

    class Meta:
        model = BasicUser
        fields = ('id', 'email', 'fio', 'password', 'aws_access_key_id', 'aws_secret_access_key', 'aws_session_token')
        write_once_fields = ('password',)
        read_only_fields = ('id',)

    def create(self, validated_data):
        password = validated_data.pop('password')
        user = BasicUser.objects.create(**validated_data)
        user.set_password(password)
        user.save()
        return user


class UserShowSerializer(serializers.ModelSerializer):
    class Meta:
        model = BasicUser
        fields = ('id', 'email', 'fio',)
        read_only_fields = ('id', 'email', 'fio',)


class CustomTokenObtainPairSerializer(TokenObtainPairSerializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    @classmethod
    def get_token(cls, user):
        token = super().get_token(user)
        token['fio'] = user.fio
        return token
