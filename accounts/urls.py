from django.urls import path, include
from rest_framework import routers
from rest_framework_simplejwt.views import TokenRefreshView

from accounts.views import UserView, CustomTokenObtainPairView

router = routers.DefaultRouter()
router.register('users', UserView)

urlpatterns = [
    path(r'token/', CustomTokenObtainPairView.as_view()),
    path(r'token/refresh/', TokenRefreshView.as_view()),
    path(r'', include('rest_framework.urls')),
]
