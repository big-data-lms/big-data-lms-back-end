from django.contrib.auth.models import AbstractUser
from django.db import models

from accounts.validators import FIOValidator


class BasicUser(AbstractUser):
    fio = models.CharField(max_length=100, verbose_name='ФИО',
                           validators=[FIOValidator],)
    aws_access_key_id = models.CharField(max_length=100, null=True)
    aws_secret_access_key = models.CharField(max_length=100, null=True)
    aws_session_token = models.CharField(max_length=500, null=True)

    last_name = None
    first_name = None

    class Meta:
        verbose_name = "Пользователь"
        verbose_name_plural = "Пользователи"

    def get_full_name(self):
        return self.fio

    def get_short_name(self):
        """Return the name for the user."""
        parts = self.fio.strip().split()
        if len(parts) > 1:
            return parts[1]
        return self.fio
