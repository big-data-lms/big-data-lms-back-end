from django.core import validators
from django.utils.translation import gettext_lazy as _


class FIOValidator(validators.RegexValidator):
    regex = r'^ *([^\W_\d]|-)+( +([^\W_\d]|-)+){0,} *$'
    message = _(
        'Введите валидное ФИО. ФИО может содержать только буквы, дефис и пробелы'
    )
    flags = 0
