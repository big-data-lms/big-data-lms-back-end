from rest_framework import viewsets, permissions
from rest_framework_simplejwt.views import TokenObtainPairView

from accounts.models import BasicUser
from accounts.permissions import is_owner_or_read_only
from accounts.serializers import UserShowSerializer, UserSerializer, CustomTokenObtainPairSerializer


class UserView(viewsets.ModelViewSet):
    queryset = BasicUser.objects.all()

    def get_serializer_class(self):
        if self.action in ('list', 'retrieve'):
            return UserShowSerializer
        return UserSerializer

    def get_permissions(self):
        if self.action == 'create':
            return [permissions.AllowAny()]
        if self.action == 'update' or self.action == 'partial_update' or self.action:
            return is_owner_or_read_only(lambda obj: obj)(),
        return super().get_permissions()


class CustomTokenObtainPairView(TokenObtainPairView):
    serializer_class = CustomTokenObtainPairSerializer
