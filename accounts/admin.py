from django.contrib import admin

from accounts.models import BasicUser

admin.site.register(BasicUser)
